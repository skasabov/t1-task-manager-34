package ru.t1.skasabov.tm.command.system;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.skasabov.tm.api.model.ICommand;
import ru.t1.skasabov.tm.command.AbstractCommand;

import java.util.Collection;

@NoArgsConstructor
public final class ApplicationCommandListCommand extends AbstractSystemCommand {

    @NotNull private static final String NAME = "commands";

    @NotNull private static final String DESCRIPTION = "Show command list.";

    @NotNull private static final String ARGUMENT = "-cmd";

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[COMMANDS]");
        @NotNull final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        for (@Nullable final ICommand command: commands) {
            if (command == null) continue;
            @Nullable final String name = command.getName();
            if (name == null || name.isEmpty()) continue;
            System.out.println(name);
        }
    }

}
