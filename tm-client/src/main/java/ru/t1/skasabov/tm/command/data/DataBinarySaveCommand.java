package ru.t1.skasabov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.skasabov.tm.dto.request.DataBinarySaveRequest;

public final class DataBinarySaveCommand extends AbstractDataCommand {

    @NotNull private static final String NAME = "data-save-bin";

    @NotNull private static final String DESCRIPTION = "Save data to binary file.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[DATA BINARY SAVE]");
        @NotNull final DataBinarySaveRequest request = new DataBinarySaveRequest();
        request.setToken(getToken());
        getDomainEndpoint().saveDataBinary(request);
    }

}
