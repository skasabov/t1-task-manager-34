package ru.t1.skasabov.tm.repository;

import ru.t1.skasabov.tm.api.repository.ISessionRepository;
import ru.t1.skasabov.tm.model.Session;

public final class SessionRepository extends AbstractUserOwnedRepository<Session> implements ISessionRepository {

}
