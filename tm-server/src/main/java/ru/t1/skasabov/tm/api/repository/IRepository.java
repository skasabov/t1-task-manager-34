package ru.t1.skasabov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.skasabov.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IRepository<M extends AbstractModel> {

    @NotNull
    List<M> findAll();

    @NotNull
    List<M> findAll(@NotNull Comparator<M> comparator);

    @NotNull
    M add(@NotNull M model);

    @NotNull
    Collection<M> add(@NotNull Collection<M> models);

    @NotNull
    Collection<M> set(@NotNull Collection<M> models);

    @NotNull
    Boolean existsById(@NotNull String id);

    @Nullable
    M findOneById(@Nullable String id);

    @NotNull
    M findOneByIndex(@NotNull Integer index);

    int getSize();

    @Nullable
    M removeOne(@NotNull M model);

    @Nullable
    M removeOneById(@NotNull String id);

    @Nullable
    M removeOneByIndex(@NotNull Integer index);

    void removeAll(@NotNull Collection<M> collection);

    void removeAll();

}
