package ru.t1.skasabov.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.skasabov.tm.enumerated.Status;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectChangeStatusByIndexRequest extends AbstractUserRequest {

    @Nullable private Integer index;

    @NotNull private Status status;

    public ProjectChangeStatusByIndexRequest(@Nullable final Integer index, @NotNull final Status status) {
        this.index = index;
        this.status = status;
    }

}
