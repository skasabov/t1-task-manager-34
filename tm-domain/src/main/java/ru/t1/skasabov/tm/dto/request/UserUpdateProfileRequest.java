package ru.t1.skasabov.tm.dto.request;

import org.jetbrains.annotations.Nullable;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public final class UserUpdateProfileRequest extends AbstractUserRequest {

    @Nullable private String lastName;

    @Nullable private String firstName;

    @Nullable private String middleName;

    public UserUpdateProfileRequest(
            @Nullable final String lastName,
            @Nullable final String firstName,
            @Nullable final String middleName
    ) {
        this.lastName = lastName;
        this.firstName = firstName;
        this.middleName = middleName;
    }

}
